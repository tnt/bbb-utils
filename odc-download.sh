#!/bin/bash

set -e

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

CONFIG="${SCRIPT_DIR}/odc-config.sh"
if [ ! -f "${CONFIG}" ]; then
	echo "[!] Missing config file odc-config.sh"
fi
source "${CONFIG}"

SLUG="$1"
MEETING_ID="$2"

# Param check
if [ "${SLUG}" == "" ]; then
	echo "[!] Missing slug"
	exit 1;
fi

if [ "${MEETING_ID}" == "" ]; then
	echo "[!] Missing meeting ID"
	exit 1;
fi

if [ "${ODC_BASE_PATH}" == "" ]; then
	echo "[!] Missing base directory"
	exit 1;
fi

# Raw BBB recording data
"${SCRIPT_DIR}/bbb-download.py" -u "https://meeting5.franken.de" -m "${MEETING_ID}" -d "${ODC_BASE_PATH}/${SLUG}/bbb.raw/"

# Process video/audio into formats for resolve
mkdir "${ODC_BASE_PATH}/${SLUG}/bbb.proc/"

if [ -e "${ODC_BASE_PATH}/${SLUG}/bbb.raw/webcams.webm" ]; then
	ffmpeg -i "${ODC_BASE_PATH}/${SLUG}/bbb.raw/webcams.webm"   -vn -c:a flac -sample_fmt s16 "${ODC_BASE_PATH}/${SLUG}/bbb.proc/audio.flac"
	ffmpeg -i "${ODC_BASE_PATH}/${SLUG}/bbb.raw/webcams.webm"   -an -c:v copy                 "${ODC_BASE_PATH}/${SLUG}/bbb.proc/webcams-vp9.mkv"
fi

if [ -e "${ODC_BASE_PATH}/${SLUG}/bbb.raw/deskshare.webm" ]; then
	ffmpeg -i "${ODC_BASE_PATH}/${SLUG}/bbb.raw/deskshare.webm" -an -c:v copy                 "${ODC_BASE_PATH}/${SLUG}/bbb.proc/deskshare-vp9.mkv"
fi
