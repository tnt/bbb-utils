#!/bin/bash

set -e

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

CONFIG="${SCRIPT_DIR}/odc-config.sh"
if [ ! -e "${CONFIG}" ]; then
	echo "[!] Missing config file odc-config.sh"
fi
source "${CONFIG}"

ODC_TYPE="$1"
ODC_SLUG="$2"

if [ "${ODC_TYPE}" == "" ]; then
	echo "[!] Missing call type (osmodevcall or retronetcall)"
	exit 1;
fi

if [ "${ODC_SLUG}" == "" ]; then
	echo "[!] Missing ODC slug"
	exit 1;
fi

if [ "${ODC_RENDER_PATH}" == "" ]; then
	echo "[!] Missing render directory"
	exit 1;
fi

ODC_RENDER_MASTER="${ODC_RENDER_PATH}/${ODC_TYPE}-${ODC_SLUG}_master.mov"

if [ ! -f "${ODC_RENDER_MASTER}" ]; then
	echo "[!] Missing master render"
	exit 1;
fi

: "${FFMPEG:=ffmpeg}"

"${FFMPEG}" \
	-hwaccel cuda -hwaccel_output_format cuda \
	-i "${ODC_RENDER_MASTER}" \
	-c:v h264_nvenc -b:v 1M \
	-c:a aac -b:a 96k \
	"${ODC_RENDER_PATH}/${ODC_TYPE}-${ODC_SLUG}_h264_420.mp4"

"${FFMPEG}" \
	-hwaccel cuda -hwaccel_output_format cuda \
	-i "${ODC_RENDER_MASTER}" \
	-c:v hevc_nvenc -b:v 512k \
	-c:a aac -b:a 96k \
	"${ODC_RENDER_PATH}/${ODC_TYPE}-${ODC_SLUG}_h265_420.mp4"

#"${FFMPEG}" \
#	-i "${ODC_RENDER_MASTER}" \
#	-c:v libvpx-vp9 -b:v 400k \
#	-c:a libopus -b:a 80k \
#	"${ODC_RENDER_PATH}/${ODC_TYPE}-${ODC_SLUG}_vp9.webm"

"${FFMPEG}" \
	-i "${ODC_RENDER_MASTER}" \
	-c:v libsvtav1 -crf 30 -b:v 2M -g 240 -svtav1-params "fast-decode=1:tune=0" \
	-c:a libopus -b:a 80k \
	"${ODC_RENDER_PATH}/${ODC_TYPE}-${ODC_SLUG}_av1.webm"
