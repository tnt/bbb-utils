#!/usr/bin/env python3

import bbb

REF_SIZE=(1600,901)

def keyframes_from_events(pze, conv_fn, merge_threshold=1.5, fps=24):

	rv = []
	le = None

	for e in pze:
		# First ?
		if le is None:
			le = e
			rv.append((0, conv_fn(e)))
			continue

		# How much time elapsed ?
		if e.ts - le.ts > merge_threshold:
			rv.append( (int(round((e.ts * fps))) - 1, conv_fn(le)) )
			rv.append( (int(round((e.ts * fps))), conv_fn(e)) )

		else:
			rv.pop()
			rv.append( (int(round(( e.ts * fps))), conv_fn(e)) )

		# Last event
		le = e
	
	return rv


#  "Pan" : floating point values from -4.0*width to 4.0*width
#  "Tilt" : floating point values from -4.0*height to 4.0*height
#  "ZoomX" : floating point values from 0.0 to 100.0
#  "ZoomY" : floating point values from 0.0 to 100.0

def to_xform(e):
	sz = REF_SIZE[0] / e.w
	cx = sz * ( 0.5 -  e.x        / REF_SIZE[0])
	cy = sz * (-0.5 + (e.y + e.h) / REF_SIZE[1])
	return (cx, cy, sz)


def animate_transform(filename, xform, limits=(0,None)):
	# Load raw events
	pze = bbb.parse_panzooms_xml(filename)

	# Limits
	pze = pze[limits[0]:limits[1]]

	# Convert to keyframes 
	kf = keyframes_from_events(pze, to_xform)
	print(kf)

	# Apply to tool
	tool = comp.FindTool(xform)

		# Switching Center/Size to animation will create a keyframe
		# at the current time, so go to first time
	comp.CurrentTime = kf[0][0]

	tool.Center = comp.Path()
	tool.Size   = comp.BezierSpline()

	for ts, (cx, cy, sz) in kf:
		tool.Center[ts] = [cx, cy]
		tool.Size[ts] = sz

# Edit the following as needed and run using comp.RunScript in the Fusion console
print("Running ...")
animate_transform('panzooms.xml', 'Transform1')
print("Done")
